let activeObject = null;
let sourceCanvas = null;
let objects = {};

function generateRandomCanvas(){
    const canvasCount = Math.floor(Math.random() * 3) + 3;  
    const divWithCanvases = document.getElementById("divWithCanvases");
    divWithCanvases.innerHTML = "";
    const selectBoxWithCanvasesId = document.getElementById("selectBoxWithCanvasesId");
    selectBoxWithCanvasesId.options.length = 0;
    let canvasOptions = '';

    // Display Options
    [].forEach.call(document.querySelectorAll('.hiddenElement'), function (el) {    
        el.style.display = 'block';
    });

    for (let i = 0; i < canvasCount; i++) {
        let newCanvas = document.createElement("canvas");
        let canvasId = `canvas${i}`;
        newCanvas.id = canvasId;
        divWithCanvases.appendChild(newCanvas);
        selectBoxWithCanvasesId.options[selectBoxWithCanvasesId.options.length] = new Option(canvasId, canvasId);

        let canvasObj = new fabric.Canvas(newCanvas, {width: 500, height:300});
        document.getElementById(canvasId).fabric = canvasObj;
        canvasObj.on('mouse:move', (options) => {
            if (!options.target && activeObject && sourceCanvas && canvasObj.lowerCanvasEl.id != sourceCanvas.lowerCanvasEl.id) {
                let cloneActiveObj;
                if (activeObject.top < 0) {
                    activeObject.top = Math.abs(activeObject.top);
                }
                activeObject.top = Math.abs(300 - activeObject.top);
                activeObject.clone((obj) => cloneActiveObj = obj);
                canvasObj.add(cloneActiveObj);
                sourceCanvas.remove(activeObject);
                sourceCanvas = null;
              }
        });
        canvasObj.observe("object:moving", (event) => {
            if (event.target.top > 300 || event.target.top < 0) {
                activeObject = canvasObj.getActiveObject();
                sourceCanvas = canvasObj;
            }
        });
    }


}

function generateRandomShapes() {
    const selectedCanvas = document.getElementById('selectBoxWithCanvasesId').value;
    const canvasObj = document.getElementById(selectedCanvas).fabric;
    const shapes = document.getElementById('shapes').value;
    let shapeObj;
    if (shapes == 'rect') {
        shapeObj = new fabric.Rect({
            width: 100,
            height: 100,
            fill: 'blue',
            opacity: 1,
            left: 0,
            top: 0
        });

    } else {
        shapeObj = new fabric.Circle({
            radius: 60,
            fill: 'red',
            left: 100,
            top: 100
        });
    }

    if (canvasObj.getObjects().length > 0) {
        console.log("Hello");
        objects[selectedCanvas].push(shapeObj);
        let group = new fabric.Group(objects[selectedCanvas], {
            left: 50,
            top: 50,
            angle: 0
        });
        
        canvasObj.clear();
        canvasObj.add(group);
    } else {
        objects = {[selectedCanvas]: [shapeObj]};
        canvasObj.add(shapeObj); 
    }

}